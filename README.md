## BCE Report

* **Length** - 40 Page
* **Batch** - B1
* **Topic** - Motivation
* **Leader** - Sagar Pathare 
* **Members** - 
    * **Sagar Pathare**
    * Gautam Ojha 
    * Gauraja Parab 
    * Surabhi Pandey 
    * Aditya Rai 
    * Vivek Saroj


----

## How to Contribute

1. Refer Content and Body Allocation.
2. Create a new text file with your content title(eg. Abstract.txt) and save contents in it. Update contents as necessary.
3. Do not make major changes in a single commit.

**Note**- Do not merge all contents into single file now. It will be more convenient to modify individual contents.

